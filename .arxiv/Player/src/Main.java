public class Main {
	
	public static void main(String[] args) {
		GoogleTextToSpeech gtts = new GoogleTextToSpeech();
		gtts.say("Hello dear friends!", "en");
		gtts.say("Bonjour mon amis!", "fr");
		gtts.say("Приветик босота, чего пришли на мой сайт?", "ru");
	}
}
