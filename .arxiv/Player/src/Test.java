import java.net.URL;
import java.net.URLConnection;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class Test {

    public static void main(String[] args) {
        Player player = null;

        try {
            //FileInputStream fis = new FileInputStream("C:\\WEB-Mornings.mp3");
            //BufferedInputStream bis = new BufferedInputStream(fis);

            URLConnection urlConnection = new URL ( "http://radio.flex.ru:8000/radionami" ).openConnection ();
            urlConnection.connect ();

            player = new Player(urlConnection.getInputStream ());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            player.play();
        } catch (JavaLayerException e) {
            System.out.println(e.getMessage());
        }
    }
}