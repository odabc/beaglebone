package wjad;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@WebServlet("/")
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("odabc.pp.ua");
        sb.append("<br>");
        try {
            Process proc = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c", "date; uname -a"});
            proc.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

            String s = "";
            while ((s = reader.readLine())!= null) {
                sb.append("<small>");
                sb.append(s);
                sb.append("<br></small>");
            }
        } catch (InterruptedException e) {
            sb.append("Error");
        }

        req.setAttribute("name", sb.toString());

        req.getRequestDispatcher("mypage.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        super.doPost(req, resp);
    }

}