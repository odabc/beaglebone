<html>
<head>
<title></title>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
</head>
<body>
<%
	String st = request.getParameter("url"); 	
	if (st == null) {
		out.print("usage: fout.jsp?url=/adress for out like textarea/");
		return;
	}
	String nm = st.substring(st.lastIndexOf("/"), st.length());
	out.print("<h3>" + nm + "</h3>");
%>
<textarea  readonly style="width: 100%; height: 390px; resize: none">
<%

	URL url = new URL(st);
    BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
	
    StringBuilder sb = new StringBuilder();
    String line;
    while((line = reader.readLine())!= null){
        sb.append(line+"\r\n");
    }
    out.print(sb.toString()); 
%>
</textarea>
</body>
</html>